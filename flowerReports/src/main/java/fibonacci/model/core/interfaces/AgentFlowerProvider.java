package fibonacci.model.core.interfaces;

import java.util.Map;

public interface AgentFlowerProvider {
	Integer getTotalCalls(long callCenterId);
	Map<String,Long> getTotalCallsByAgent(long callCenterId);
	Map<String,Long> getSalesCallsByAgent(long callCenterId);
	Map<String,Double> getSalesPercentageByTag(long callCenterId, String agent);
	long getSalesCallsPerAgent(long callCenterId, String agentName);
	
}
