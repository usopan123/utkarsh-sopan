package fibonacci.model.core.interfaces;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kairosdb.client.HttpClient;
import org.kairosdb.client.builder.AggregatorFactory;
import org.kairosdb.client.builder.DataFormatException;
import org.kairosdb.client.builder.QueryBuilder;
import org.kairosdb.client.builder.TimeUnit;
import org.kairosdb.client.builder.grouper.TagGrouper;
import org.kairosdb.client.response.QueryResponse;
import org.kairosdb.client.response.Results;
import org.springframework.beans.factory.annotation.Autowired;

public class LeadFlowerProviderImpl implements SalesFlowerProvider {
	@Autowired
	private HttpClient client;
	@Override
	public Integer getTotalSales(long callCenterId) {
		QueryBuilder builder = QueryBuilder.getInstance();
		builder.setStart(1, TimeUnit.MONTHS)
		       .addMetric("agentflowertestsalescalls" + callCenterId)
		       .addAggregator(AggregatorFactory.createSumAggregator(1, TimeUnit.MONTHS));
//		       .addGrouper(new TagGrouper("agentName","projectName","leadTag","month"));
		try {
			QueryResponse response = client.query(builder);
			List<Results> results  = response.getQueries().get(0).getResults();
			for(Results result : results) {
				try {
					return (int) result.getDataPoints().get(0).longValue();
				}
				catch (DataFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Map<String, Long> getTotalSalesByTag(long callCenterId) {
		QueryBuilder builder = QueryBuilder.getInstance();
		builder.setStart(1, TimeUnit.MONTHS)
		       .addMetric("agentflowertestsalescalls" + callCenterId)
		       .addAggregator(AggregatorFactory.createSumAggregator(1, TimeUnit.MONTHS))
		       .addGrouper(new TagGrouper("leadTagName"));
		Map<String,Long> resultMap = new HashMap<String,Long>();
		try {
			QueryResponse response = client.query(builder);
			List<Results> results  = response.getQueries().get(0).getResults();
			
			for(Results result : results) {
				try {
				     String agentName = result.getTags().get("leadTagName").get(0);
				     long value = result.getDataPoints().get(0).longValue();
				     resultMap.put(agentName, value);
				}
				catch (DataFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	public Map<String, Double> getSalesPercentageByAgent(long callCenterId, String tagName) {
		QueryBuilder builder = QueryBuilder.getInstance();
		builder.setStart(1, TimeUnit.MONTHS)
		       .addMetric("agentflowertestsalescalls" + callCenterId)
		       .addAggregator(AggregatorFactory.createSumAggregator(1, TimeUnit.MONTHS))
		       .addTag("leadTagName", tagName)
		       .addGrouper(new TagGrouper("agentName","leadTagName"));
		
		Map<String,Double> resultMap = new HashMap<String,Double>();
		Long salesValue = getSalesPerTag(callCenterId, tagName);
		try {
			QueryResponse response = client.query(builder);
			List<Results> results  = response.getQueries().get(0).getResults();
			
			for(Results result : results) {
				try {
				     String agentName = result.getTags().get("agentName").get(0);
				     Double value = (result.getDataPoints().get(0).doubleValue()*100)/salesValue.doubleValue();
				     resultMap.put(agentName, value);
				}
				catch (DataFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	public long getSalesPerTag(long callCenterId, String tagName) {
		QueryBuilder builder = QueryBuilder.getInstance();
		builder.setStart(1, TimeUnit.MONTHS)
		       .addMetric("agentflowertestsalescalls" + callCenterId)
		       .addAggregator(AggregatorFactory.createSumAggregator(1, TimeUnit.MONTHS))
		       .addGrouper(new TagGrouper("leadTagName"))
		       .addTag("leadTagName", tagName);
		try {
			QueryResponse response = client.query(builder);
			List<Results> results  = response.getQueries().get(0).getResults();
			for(Results result : results) {
				try {
					return (int) result.getDataPoints().get(0).longValue();
				}
				catch (DataFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
