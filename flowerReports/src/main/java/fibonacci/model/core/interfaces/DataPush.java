package fibonacci.model.core.interfaces;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.apache.commons.lang3.RandomUtils;
import org.kairosdb.client.HttpClient;
import org.kairosdb.client.builder.MetricBuilder;
import org.kairosdb.client.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataPush {
	@Autowired
	private HttpClient client;

	public static String[] agentNames = {"3clogicTestAgent1","3clogicTestAgent2","Aaron Mosier" ,"Aaron Zubrzycki" ,"Adolfo Cazares" ,"Adrian Gomez" ,"Adriana Solis" ,"Adrienne Kilsdonk","Alaina Lechuga" ,"Alan Sheppard" ,"Albert Bandin" ,"Albert Stately" ,"Alex Long"    ,"Alexander Hernandez","Alexandria Mehls","Ali Gow"    ,"Alice Hartnell" ,"Alice Valenzuela","Alice Waltz"    ,"Alix Avila"    ,"Ally Parmeter" ,"Allyson Beck" ,"Amanda Pawloski" ,"Amanda Pawlosky" ,"Amber Fisher" ,"Amber Kandoll" ,"Amber Wallace" ,"Amber Williams" ,"America Moreno" ,"Amy Thomas"    ,"Ana Campos"    ,"Ana Torres"    ,"Andrea Cordova" ,"Andrea E Halvorson","Andrea Rugg"    ,"Andrea Salas" ,"Andrew Castro" ,"Andrew Espinosa" ,"Anel Lira"    ,"Angela Flint" ,"Angela Lazzaro" ,"Angela Leyva" ,"Angelica Baldwin","Angelica Hernandez","Angelica Morales","Angelica Robles" ,"Angelica Solis" ,"Annette De La Cruz","Anthony Babcock" ,"Antone Payne" ,"Arlene Gallegos" ,"Arthur Dean"    ,"Ashley Palomino" ,"Avrianna Giordano","Ayes Ehikioya" ,"Barbara Mariani" ,"Baret Chakarian" ,"Becky Vendela" ,"Belinda Harris" ,"Benjamin Lee" ,"Bernadette Coopwood","Beth Malec"    ,"Betty Salgado" ,"Blanca Andrade" ,"Bobbie Jo Ryder" ,"Brad Rugg"    ,"Bradley Johnson" ,"Brandon Larsen" ,"Brenda Davis" ,"Brian Bartz"    ,"Brian Bartz"    ,"Brian Bartz"    ,"Brianna Johnson" ,"Brittney Burke" ,"Brock Amundson" ,"Bryan Grazier" ,"Bryan McNair" ,"Bryan Warren" ,"Callie Sturzenegger","Camecia Davis" ,"Cara Lee Wrazidlo","Carina Gomez" ,"Caris Brummer" ,"Carlton Coleman" ,"Carnell Johnson" ,"Cecilia Morelas" ,"Celia Alvarez" ,"Celia Nevarez" ,"Cesar Martinez" ,"Chaka Gipson" ,"Chantel Perez" ,"Charles Strand" ,"Charlice Buck" ,"Chase Dunbar" ,"Chelsie Lee"    ,"Cheryl Mertes" ,"Cheryl Tavakoli" ,"Chi Hong"    ,"Chris Bremer" ,"Christel Clowes"};

	public static String[] leadTagNames = { "Central", "Eastern", "Pacific", "Mountain" };

	public void pushData(long callCenterId) {
		long startTime = System.currentTimeMillis();
		int[] totalCalls = new int[1000];
		int[] agentIndex = new int[1000];
		int[] leadTagIndex = new int[1000];
		for (int i = 0; i < 1000; i++) {
			MetricBuilder builder = MetricBuilder.getInstance();
			int val = RandomUtils.nextInt(0,5);
			totalCalls[i] = val;
			int agenti= RandomUtils.nextInt(0, agentNames.length);
			agentIndex[i] = agenti;
			int leadTagi = RandomUtils.nextInt(0, leadTagNames.length);
			leadTagIndex[i] = leadTagi;
			
			builder.addMetric("agentflowertesttotalcalls" + callCenterId)
					.addTag("agentName", agentNames[agenti])
					.addTag("leadTagName", leadTagNames[leadTagi])
					.addDataPoint(startTime - (i * 300000), val );
			try {
				Response response = client.pushMetrics(builder);
				// client.shutdown();
			}
			catch (URISyntaxException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			for (int i = 0; i < 1000; i++) {
				MetricBuilder builder = MetricBuilder.getInstance();
				builder.addMetric("agentflowertestsalescalls" + callCenterId)
						.addTag("agentName", agentNames[agentIndex[i]])
						.addTag("leadTagName", leadTagNames[leadTagIndex[i]])
						.addDataPoint(startTime - (i * 5000),RandomUtils.nextInt(0,totalCalls[i]/2) );
				try {
					Response response = client.pushMetrics(builder);
					// client.shutdown();
				}
				catch (URISyntaxException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}
}
