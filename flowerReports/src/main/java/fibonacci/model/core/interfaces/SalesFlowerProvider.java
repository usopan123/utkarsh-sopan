package fibonacci.model.core.interfaces;

import java.util.Map;

public interface SalesFlowerProvider {
	Integer getTotalSales(long callCenterId);
	Map<String,Long> getTotalSalesByTag(long callCenterId);
	
	Map<String,Double> getSalesPercentageByAgent(long callCenterId, String tagName);
	long getSalesPerTag(long callCenterId, String tagName);
}
