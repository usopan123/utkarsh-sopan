package fibonacci.model.core;

public class CoreUtils {
	public static CartesianPoint getPointOnCircle(float radius, float angleInDegrees) {
		 // Convert from degrees to radians via multiplication by PI/180        
        float x = (float)(radius * Math.cos(angleInDegrees * Math.PI / 180F)) ;
        float y = (float)(radius * Math.sin(angleInDegrees * Math.PI / 180F)) ;
        CartesianPoint point = new CartesianPoint();
        point.setX(Math.round(x));
        point.setY(Math.round(y));
        return point;
		
	}
	public static CartesianPoint getPointOnCircle(float radius, float angleInDegrees, CartesianPoint point) {
		 // Convert from degrees to radians via multiplication by PI/180        
       float x = (float)(radius * Math.cos(angleInDegrees * Math.PI / 180F) + point.getX())  ;
       float y = (float)(radius * Math.sin(angleInDegrees * Math.PI / 180F)+ point.getY()) ;
       CartesianPoint point2 = new CartesianPoint();
       point2.setX(Math.round(x));
       point2.setY(Math.round(y));
       return point2;
		
	}
}
