package fibonacci.model.core.draw;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomUtils;

import fibonacci.model.core.CartesianPoint;
import fibonacci.model.core.CoreUtils;
import fibonacci.model.core.interfaces.AgentFlowerProvider;
import fibonacci.model.core.interfaces.SalesFlowerProvider;
import fibonacci.model.view.Edge;
import fibonacci.model.view.GraphView;
import fibonacci.model.view.Node;

public class FlowerDrawer {
	public enum AgentFlowerNodeType {
		total, agent, agentproject, tag, failure, sales
	}

	public static GraphView drawAgentFlower(long callCenterId, AgentFlowerProvider dataProvider) {
		GraphView graph = new GraphView();
		List<Node> nodes = new ArrayList<Node>();
		List<Edge> edges = new ArrayList<Edge>();
		Node center = new Node();
		center.setId("totalcalls");
		Integer totalCallsValue = dataProvider.getTotalCalls(callCenterId);
		center.setLabel("Total Calls:" + totalCallsValue);
		
		center.setSize(totalCallsValue.intValue()*10);
		center.setX(0);
		center.setY(0);
		center.setColor("#4285f4");
//		center.setColor();
		nodes.add(center);
		List<Node> agentNodes = new ArrayList<Node>();
		List<Edge> agentEdges = new ArrayList<Edge>();
		Map<String, Long> totalCallsByAgent = dataProvider.getTotalCallsByAgent(callCenterId);
		int i = 0;
		for (String agentName : totalCallsByAgent.keySet()) {
			Node agent = new Node();
			agent.setId(agentName);
			agent.setLabel(agentName + ":" + totalCallsByAgent.get(agentName).intValue());
			agent.setSize(totalCallsByAgent.get(agentName).intValue()*50);
//			agent.setType(AgentFlowerNodeType.agent.toString());
			CartesianPoint point = CoreUtils.getPointOnCircle(50.0f, (i * 360f) / totalCallsByAgent.keySet().size());
			agent.setX(point.getX());
			agent.setY(point.getY());
			agent.setColor("#f4b400");
			agentNodes.add(agent);

			Edge agentEdge = new Edge();
			agentEdge.setId("totalcalls" + agentName);
			agentEdge.setSource("totalcalls");
			agentEdge.setTarget(agentName);
			agentEdges.add(agentEdge);
			i++;
		}
		List<Node> salesNodes = new ArrayList<Node>();
		List<Edge> salesEdges = new ArrayList<Edge>();
		Map<String, Long> totalSalesCallsByAgent = dataProvider.getSalesCallsByAgent(callCenterId);
		int j = 0;
		for (Node agentNode : agentNodes) {
			Node sales = new Node();
			sales.setId(agentNode.getId() + "sales");
			sales.setLabel(agentNode.getId() + ":" + totalSalesCallsByAgent.get(agentNode.getId()).intValue());
			sales.setSize(totalSalesCallsByAgent.get(agentNode.getId()).intValue()*25);
//			sales.setType(AgentFlowerNodeType.sales.toString());
			sales.setColor("#db4437");
			CartesianPoint origin = new CartesianPoint();
			origin.setX(agentNode.getX());
			origin.setY(agentNode.getY());
			CartesianPoint point = CoreUtils.getPointOnCircle(20.0f, (j * 360f) / totalCallsByAgent.keySet().size(),
					origin);
			sales.setX(point.getX());
			sales.setY(point.getY());
			salesNodes.add(sales);
			Edge salesEdge = new Edge();
			salesEdge.setId(agentNode.getId() + "salesedge");
			salesEdge.setSource(agentNode.getId());
			salesEdge.setTarget(agentNode.getId() + "sales");
			salesEdges.add(salesEdge);
			j++;
		}
		List<Node> tagsNodes = new ArrayList<Node>();
		List<Edge> tagsEdges = new ArrayList<Edge>();
		int k = 0;
		for (Node salesNode : salesNodes) {
			Map<String, Double> tagData = dataProvider.getSalesPercentageByTag(callCenterId, salesNode.getId()
					.replace("sales", ""));
			for (String tagName : tagData.keySet()) {
				Node tag = new Node();
				tag.setId(salesNode.getId() + "sales"+tagName);
				tag.setLabel(salesNode.getId() + ":" + tagName + ":" + tagData.get(tagName));
				tag.setSize(tagData.get(tagName).intValue()*50);
//				tag.setType(AgentFlowerNodeType.tag.toString());
				tag.setColor("#0f9d58");
				CartesianPoint origin = new CartesianPoint();
				origin.setX(salesNode.getX());
				origin.setY(salesNode.getY());
				CartesianPoint point = CoreUtils.getPointOnCircle(5.0f, 90 + ((k * 360f)
						/ tagData.keySet().size()), origin);
				tag.setX(point.getX());
				tag.setY(point.getY());
				tagsNodes.add(tag);
				Edge agentEdge = new Edge();
				agentEdge.setId(salesNode.getId()+tagName + "edge");
				agentEdge.setSource(salesNode.getId());
				agentEdge.setTarget(salesNode.getId() + "sales"+tagName);
				tagsEdges.add(agentEdge);
				k++;
			}
		}
		nodes.addAll(tagsNodes);
		nodes.addAll(salesNodes);
		nodes.addAll(agentNodes);
		edges.addAll(agentEdges);
		edges.addAll(tagsEdges);
		edges.addAll(salesEdges);
		graph.setEdges(edges);
		graph.setNodes(nodes);
		return graph;

	}
	public static GraphView drawSalesFlower(long callCenterId, SalesFlowerProvider dataProvider) {
		GraphView graph = new GraphView();
		List<Node> nodes = new ArrayList<Node>();
		List<Edge> edges = new ArrayList<Edge>();
		Node center = new Node();
		center.setId("totalcalls");
		Integer totalSales = dataProvider.getTotalSales(callCenterId);
		center.setLabel("Sales:"+totalSales);
		
		center.setSize(totalSales *100);
		center.setX(0);
		center.setY(0);
		center.setColor("#4285f4");
//		center.setType(AgentFlowerNodeType.total.toString());
		nodes.add(center);
		List<Node> agentNodes = new ArrayList<Node>();
		List<Edge> agentEdges = new ArrayList<Edge>();
		Map<String, Long> totalCallsByAgent = dataProvider.getTotalSalesByTag(callCenterId);
		int i = 0;
		for (String agentName : totalCallsByAgent.keySet()) {
			Node agent = new Node();
			agent.setId(agentName);
			agent.setLabel(agentName + ":" + totalCallsByAgent.get(agentName).intValue());
			agent.setSize(totalCallsByAgent.get(agentName).intValue()* 50);
			agent.setColor("#f4b400");
//			agent.setType(AgentFlowerNodeType.agent.toString());
			CartesianPoint point = CoreUtils.getPointOnCircle(50.0f, (i * 360f) / totalCallsByAgent.keySet().size());
			agent.setX(point.getX());
			agent.setY(point.getY());
			agentNodes.add(agent);

			Edge agentEdge = new Edge();
			agentEdge.setId("totalcalls" + agentName);
			agentEdge.setSource("totalcalls");
			agentEdge.setTarget(agentName);
			agentEdges.add(agentEdge);
			i++;
		}
		List<Node> salesNodes = new ArrayList<Node>();
		List<Edge> salesEdges = new ArrayList<Edge>();
//		Map<String, Long> totalSalesCallsByAgent = dataProvider.getTotalSalesByTag(callCenterId);
//		int j = 0;
//		for (Node agentNode : agentNodes) {
//			Node sales = new Node();
//			sales.setId(agentNode.getId() + "sales");
//			sales.setLabel(agentNode.getId() + ":" + totalSalesCallsByAgent.get(agentNode.getId()).intValue());
//			sales.setSize(totalSalesCallsByAgent.get(agentNode.getId()).intValue());
//			sales.setType(AgentFlowerNodeType.sales.toString());
//			CartesianPoint origin = new CartesianPoint();
//			origin.setX(agentNode.getX());
//			origin.setY(agentNode.getY());
//			CartesianPoint point = CoreUtils.getPointOnCircle(20.0f, (j * 360f) / totalCallsByAgent.keySet().size(),
//					origin);
//			sales.setX(point.getX());
//			sales.setY(point.getY());
//			salesNodes.add(sales);
//			Edge salesEdge = new Edge();
//			salesEdge.setId(agentNode.getId() + "salesedge");
//			salesEdge.setSource(agentNode.getId());
//			salesEdge.setTarget(agentNode.getId() + "sales");
//			salesEdges.add(salesEdge);
//			j++;
//		}
		List<Node> tagsNodes = new ArrayList<Node>();
		List<Edge> tagsEdges = new ArrayList<Edge>();
		int k = 0;
		for (Node salesNode : agentNodes) {
			Map<String, Double> tagData = dataProvider.getSalesPercentageByAgent(callCenterId, salesNode.getId());
			for (String tagName : tagData.keySet()) {
				Node tag = new Node();
				tag.setId(salesNode.getId() + "sales"+tagName);
				tag.setLabel(salesNode.getId() + ":" + tagName + ":" + tagData.get(tagName));
				tag.setSize(tagData.get(tagName).intValue()*25);
				tag.setColor("0f9d58");
//				tag.setType(AgentFlowerNodeType.tag.toString());
				CartesianPoint origin = new CartesianPoint();
				origin.setX(salesNode.getX());
				origin.setY(salesNode.getY());
				CartesianPoint point = CoreUtils.getPointOnCircle(20.0f, (k * 360f)
						/ tagData.keySet().size(), origin);
				tag.setX(point.getX());
				tag.setY(point.getY());
				tagsNodes.add(tag);
				Edge agentEdge = new Edge();
				agentEdge.setId(salesNode.getId()+tagName + "edge");
				agentEdge.setSource(salesNode.getId());
				agentEdge.setTarget(salesNode.getId() + "sales"+tagName);
				tagsEdges.add(agentEdge);
				k++;
			}
		}
		nodes.addAll(tagsNodes);
//		nodes.addAll(salesNodes);
		nodes.addAll(agentNodes);
		edges.addAll(agentEdges);
		edges.addAll(tagsEdges);
//		edges.addAll(salesEdges);
		graph.setEdges(edges);
		graph.setNodes(nodes);
		return graph;

	}
}
