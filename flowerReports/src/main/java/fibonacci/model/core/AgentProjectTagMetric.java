package fibonacci.model.core;

import lombok.Data;

@Data
public class AgentProjectTagMetric {
	private String agentName;
	private String projectName;
	private String tag;
	private long totalCalls;
	private long totalSales;
	private String month;
	
}
