package fibonacci.model.core;

import lombok.Data;

@Data
public class CartesianPoint {
	private int x;
	private int y;
}
