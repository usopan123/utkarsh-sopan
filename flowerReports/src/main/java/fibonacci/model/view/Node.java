package fibonacci.model.view;

import lombok.Data;

@Data
public class Node {
	private String id;
	private String label;
	private int x;
	private int y;
	private int size;
//	private String type;
	private String color;
}
