package fibonacci.model.view;

import lombok.Data;

@Data
public class Edge {
	private String id;
	private String source;
	private String target;
}
