package fibonacci.model.view;

import java.util.List;

import lombok.Data;

@Data
public class GraphView {
	private List<Node> nodes;
	private List<Edge> edges;
}
