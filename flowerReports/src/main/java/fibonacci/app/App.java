package fibonacci.app;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomUtils;
import org.kairosdb.client.HttpClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fibonacci.model.core.CartesianPoint;
import fibonacci.model.core.CoreUtils;
import fibonacci.model.core.interfaces.AgentFlowerProvider;
import fibonacci.model.core.interfaces.AgentFlowerProviderImpl;
import fibonacci.model.core.interfaces.DataPush;
import fibonacci.model.core.interfaces.LeadFlowerProviderImpl;
import fibonacci.model.core.interfaces.SalesFlowerProvider;
import fibonacci.model.view.Edge;
import fibonacci.model.view.GraphView;
import fibonacci.model.view.Node;

@SpringBootApplication
@RestController
@RequestMapping("/data")
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
	@RequestMapping("/test")
	public GraphView getTestData() {
		GraphView graph = new GraphView();
		List<Node> nodes = new ArrayList<Node>();
		List<Edge> edges = new ArrayList<Edge>();
		Random random = new Random(System.currentTimeMillis());
		Node node = new Node();
		node.setId("n"+0);
		node.setLabel("node"+0);
		node.setSize(random.nextInt(100) + 1);
		int x = RandomUtils.nextInt(0, 40) - 20;			
		node.setX(0);
		node.setY(0);
		node.setType("square");
		nodes.add(node);
		
		for(int i = 0; i< 10; i++) {
			Node node1 = new Node();
			node1.setId("n"+(i+1));
			node1.setLabel("node"+i);
			node1.setSize(10);
			int x2 = RandomUtils.nextInt(0, 40);
			CartesianPoint point = CoreUtils.getPointOnCircle(50.0f, 360f/i+1);
			node1.setX(point.getX());
			node1.setY(point.getY());
			node1.setType("square");
			nodes.add(node1);
			Edge edge = new Edge();
			edge.setId("e" + i);
			edge.setSource("n0");
			if(i<10) {
				edge.setTarget("n"+(i+1));
				edges.add(edge);
			}
			
	}
		graph.setEdges(edges);
		graph.setNodes(nodes);
		try {
			System.out.println(new ObjectMapper().writeValueAsString(graph));
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return graph;
	}
	@Bean
	public HttpClient client() {
		HttpClient client = null;
		try {
			client = new HttpClient("http://104.131.180.135:8080");
		}
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return client;
	}
	
	@Bean
	public DataPush dataPush() {
		DataPush dataPush = new DataPush();
		return dataPush;
	}
	@Bean
	public AgentFlowerProvider agentFlowerProvider(){
		AgentFlowerProvider agentFlowerProvider = new AgentFlowerProviderImpl();
		return agentFlowerProvider;
	}
	@Bean
	public SalesFlowerProvider salesFlowerProvider(){
		SalesFlowerProvider agentFlowerProvider = new LeadFlowerProviderImpl();
		return agentFlowerProvider;
	}
}
