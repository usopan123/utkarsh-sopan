package fibonacci.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fibonacci.model.core.draw.FlowerDrawer;
import fibonacci.model.core.interfaces.AgentFlowerProvider;
import fibonacci.model.core.interfaces.SalesFlowerProvider;
import fibonacci.model.view.GraphView;

@RestController
@RequestMapping("/graph")
public class DataController {
@Autowired
private AgentFlowerProvider agentFlowerProvider;
@Autowired
private SalesFlowerProvider salesFlowerProvider;

@RequestMapping("/agent")
public GraphView getData(){
	return FlowerDrawer.drawAgentFlower(2, agentFlowerProvider);
}
@RequestMapping("/sales")
public GraphView getSalesData(){
	return FlowerDrawer.drawSalesFlower(2, salesFlowerProvider);
}
}
