package fibonacci;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.junit.Test;
import org.kairosdb.client.HttpClient;
import org.kairosdb.client.builder.AggregatorFactory;
import org.kairosdb.client.builder.Grouper;
import org.kairosdb.client.builder.MetricBuilder;
import org.kairosdb.client.builder.QueryBuilder;
import org.kairosdb.client.builder.TimeUnit;
import org.kairosdb.client.builder.grouper.TagGrouper;
import org.kairosdb.client.builder.grouper.ValueGrouper;
import org.kairosdb.client.response.QueryResponse;
import org.kairosdb.client.response.Response;

import com.fasterxml.jackson.databind.ObjectMapper;

public class KariosDBClientTest {
@Test
public void putMetricTest() {
	MetricBuilder builder = MetricBuilder.getInstance();
	builder.addMetric("metric1")
	        .addTag("host", "server2")
	        .addTag("customer", "Acme")
	        .addDataPoint(System.currentTimeMillis(), 10)
	        .addDataPoint(System.currentTimeMillis(), 30L);
	HttpClient client = null;
	try {
		client = new HttpClient("http://104.131.180.135:8080");
	}
	catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		Response response = client.pushMetrics(builder);
		client.shutdown();
	}
	catch (URISyntaxException | IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
@Test
public void getMetricTest() {
	QueryBuilder builder = QueryBuilder.getInstance();
	builder.setStart(1, TimeUnit.HOURS)
	       .addMetric("agentflowertesttotalcalls1")
	       .addAggregator(AggregatorFactory.createSumAggregator(1, TimeUnit.MONTHS))
           .addGrouper(new TagGrouper("agentName","leadTagName"));
//	       .addGrouper(new TagGrouper("agentName","leadTagName"));
	       
//	       .addGrouper(new TagGrouper("host")).addGrouper(new ValueGrouper(2));
	HttpClient client = null;
	try {
		client = new HttpClient("http://104.131.180.135:8080");
	}
	catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		QueryResponse response = client.query(builder);
		System.out.println(new ObjectMapper().writeValueAsString(response.getQueries()));
		client.shutdown();
	}
	catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
	

}
