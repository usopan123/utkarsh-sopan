package fibonacci.model.core.interfaces;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fibonacci.app.App;
import fibonacci.model.core.interfaces.DataPush;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
public class PushDataTest {
@Autowired
private DataPush dataPush;
@Autowired
private AgentFlowerProvider flowerProvider;
@Test
public void dataPushTest() {
	dataPush.pushData(2);
}
@Test
public void dataQueryTest() {
	System.out.println(flowerProvider.getTotalCalls(1));
}
@Test
public void dataQueryTest2() {
	System.out.println(flowerProvider.getTotalCallsByAgent(1));
}
@Test
public void dataQueryTest3() {
	System.out.println(flowerProvider.getSalesCallsByAgent(1));
}
@Test
public void dataQueryTest4() {
	System.out.println(flowerProvider.getSalesPercentageByTag(1, "dick"));
}
}
