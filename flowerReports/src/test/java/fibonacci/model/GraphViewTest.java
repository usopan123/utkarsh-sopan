package fibonacci.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fibonacci.model.view.Edge;
import fibonacci.model.view.GraphView;
import fibonacci.model.view.Node;

public class GraphViewTest {
@Test
public void jsonTest() {
	GraphView graph = new GraphView();
	List<Node> nodes = new ArrayList<Node>();
	List<Edge> edges = new ArrayList<Edge>();
	for(int i = 0; i< 10; i++) {
		Node node = new Node();
		node.setId("n"+i);
		node.setLabel("node"+i);
		node.setSize(10);
		node.setX(0);
		node.setY(i);
		nodes.add(node);
		Edge edge = new Edge();
		edge.setId("e" + i);
		edge.setSource("n0");
		edge.setTarget("n"+i);
		edges.add(edge);
}
	graph.setEdges(edges);
	graph.setNodes(nodes);
	try {
		System.out.println(new ObjectMapper().writeValueAsString(graph));
	}
	catch (JsonProcessingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
